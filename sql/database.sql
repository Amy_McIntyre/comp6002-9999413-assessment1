create database `Web`;

use `Web`;

create table `User`
(
    `id` int AUTO_INCREMENT,
    `username` varchar (25),
    `password` varchar (25),
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `Home`
(
    `id` int AUTO_INCREMENT,
    `title` text,
    `text` text,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `quicklinks`
(
    `id`int AUTO_INCREMENT,
    `title` text,
    `image`text,
    `text` text,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `information`
(
    `id` int AUTO_INCREMENT,
    `title` text,
    `text` text,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `pathways`
(
    `id` int AUTO_INCREMENT,
    `title` text,
    `text` text,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `navbar`
(
    `id` int AUTO_INCREMENT,
    `Home` varchar(25),
    `quicklinks` varchar(25),
    `information` varchar(25),
    `pathways` varchar(25),
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into user(`username`, `password`) values ('AmyM', 'password')

insert into Home (`title`, `text`) values ('Welcome to bcs.net.nz','Toi-Ohomai Institute of Technology & University of Waikato
                Bachelor of Computing and Mathematical Science,This Intranet is for students who are completing the Bachelor of Computer Science in Applied Science.
                 Successfully complete the Diploma in Applied Computing (Level 5) and (Level 6) and gain full credit for the first and second year of this three year University of Waikato degree')

insert into quicklinks (`title`, `image`, `text`) values ('Moodle', 'http://moodle2.boppoly.ac.nz','Moodle is where you as students will logon to find all of your course
                        information you will need such as class times, assessment dates and
                        Class information. Need help.
                        Go to the info page.')

insert into quicklinks (`title`, `image`, `text`) values ('TOI-OHOMAI', 'https://www.boppoly.ac.nz/', 'This is the main Polytechnic website where you will find all of 
                        the inforamtion about every course which includes course fees,
                        start/finish dates, what topics you will be studying in your chosen
                        course etc.')

insert into quicklinks (`title`, `image`, `text`) values ('Outlook','http://www.outlook.com/stu.boppoly.ac.nz','This is your personal polytechnic email address. You can also use your
                        own email address if you wish. For help to log onto your email address
                        go to the information page.')

insert into quicklinks (`title`, `image`, `text`) values ('Microsoft Imagine','http://e5.onthehub.com/WebStore/ProductsByMajorVersionList.aspx?ws=20830094-5e9b-e011-969d-0030487d8897','                            Microsoft Imagine previously known as Microsoft Dreamspark is where you as 
                            students are able to download software such as Visual Studio for free.
                            To learn how to log into Microsoft Imagine go to the information page.')

insert into quicklinks (`title`, `image`, `text`) values ('Office 365','https://portal.office.com/OLS/MySoftware.aspx','Get 5 Office licenses for free while you study.')

insert into quicklinks (`title`, `image`, `text`) values ('Visual Studio','https://go.microsoft.com/fwlink/?LinkId=691978&clcid=0x409','Clicking on this link will start an automatic download of visual Studio
                    community version. You will use this a lot in this course.')

insert into quicklinks (`title`, `image`, `text`) values ('Bitbucket','https://bitbucket.org/','Bitbucket is a web-based hosting service you will use when you are creating
                    projects such as websites. You can make your own account with your private email
                    but for in class use your tutor may get you to make a bcs email.')

insert into quicklinks (`title`, `image`, `text`) values ('Google Drive','https://drive.google.com/','Google drive is where you can upload and save your work and be able to access it
                    anywhere. If you log in with your BSC credentials you will have unlimited storage.
                    to find out what your credentials are go to the information page.')

insert into pathways (`title`, `text`) values("Certificate in Computing (CIC) Level 2","This course is designed to improve your digital literacy. You may be returning to the workforce or simply wanting to keep up with technology and meet and learn with others. You will use a range of software tools and applications that are freely available online to create and share digital content including images, audio and video. You will learn how to connect and collaborate with others online, and will have the opportunity to use a range of digital devices including PCs, phones and tablets. You will learn about basic troubleshooting and personal cyber-safety. ")
insert into pathways (`title`, `text`) values("New Zealand Certificate in Information Technology Essentials (Level 4)","This one semester computing course gives you a basic knowledge of computer hardware, operating systems, applications and networks so you can get your IT career off to a strong start. During your study you'll learn about the main components of a computer and explain the way in which they interact, assembly, disassembly and maintenance, troubleshooting common problems, operating systems and end user security, and internet-based services. You will also complete an introduction to computational thinking and basic programming. ")
insert into pathways (`title`, `text`) values("Diploma in Applied Computing (Level 5)","Want to get a job as a computer programmer, systems analyst, website or software developer? Technology is rapidly changing the way we live and New Zealand desperately needs more computer professionals. Level 5 and Level 6 Diplomas offer the only local pathway towards the University of Waikatoâ€™s Bachelor of Science, specialising in Applied Computing. This Level 5 Diploma in Applied Computing develops core skills in IT infrastructure, software, multimedia, networks and programming to meet a wide range of business computing needs. ")
insert into pathways (`title`, `text`) values("Diploma in Applied Computing (Level 6)","This computing and IT course develops your career as an intermediate IT professional. The Level 6 Diploma includes website development, business and project management, database systems and advanced programming. Complete just one more year after this diploma to gain the University of Waikatoâ€™s Bachelor of Science (BSc) majoring in Computer Science with a specialisation in Applied Computing, taught right here in Tauranga. Students benefit from strong industry connections and get plenty of opportunities to put theory into practice, using their creative and analytical skills to develop portfolios of their work to present to future employers ")
insert into pathways (`title`, `text`) values("Bachelor of Science (BSc) in Computer Science with Specialisation in Applied Computing","There is no need to leave Tauranga to get your computing degree. Successfully complete the Diploma in Applied Computing (Level 5) and (Level 6) and gain full credit for the first and second year of this three year University of Waikato degree, delivered in Tauranga. You'll take courses on software systems, web application and development, how people and computers interact and how to ensure systems work well and are easy to use. Throughout the degree you'll continuously put into practice what you learn through hands-on, practical projects. ")

Insert into information (`title`, `text`) Values ('About the BSC Network','The BCS network or known as Pandora Network is completely seperate from 
                        the corporate network meaning:
                        Your H: Drives in the corporate network are different to pandora labs
                       None of your resourse can be shared ')
Insert into information (`title`, `text`) Values ('Moodle Logon','  Your Login for moodle is your corporate login and this is the same for when 
                        you log onto one of the computers in the corporate labs.
                        Your login for BCS labs (Pandora) is your Student ID and the password you chose.')
Insert into information (`title`, `text`) Values ('Wifi Logon','To connect your device to the Student Wifi, Select Toi Ohomai Wi-Fi in your Wifi Settings,
                      Your Username = moodle-id@stu.boppoly.ac.nz. Yous Password = your student id (unless you have changed it)')
Insert into information (`title`, `text`) Values ('Outlook','Your email address for you outlook email account will be your corporate
                        username as well as the address "corporate-username@stu.boppoly.ac.nz"')
Insert into information (`title`, `text`) Values ('Google Apps login',' You wil login with your BSC (Pandora) credentials.
                            your email will be "student-id@bcs.net.nz"')
Insert into information (`title`, `text`) Values ('Office Login','To get Office 365 at home you will need to sign in with your corporate 
                        credentials.')
Insert into information (`title`, `text`) Values ('Microsoft Imagine Login','For imagine login you will also use your BCS (Pandora) credentials "student-id@bcs.net.nz"
                        and your chosen password. You can download the software and keep it for free.')
Insert into information (`title`, `text`) Values ('Google Chrome Sign In','To sign into google chrome, at the top right hand corner there will be a grey box.
                        Click on this box and add youself in there by signing in.
                        Use your BCS (Pandora) email "student-id@bcs.net.nz" and the password you create.
                        Once you do this Link the data and you have access to things like the bcs bookmarks etc.')