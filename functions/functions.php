<?php
    include_once('creds.php');
////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

    function connection()
    {
        $conn = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);
        if($conn->connect_errno > 0)
        {
            echo('Unable to connect to the database[' .$conn->connect_errno. ']');
        }
        return $conn;
    }

    function loginAdmin() 
{

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM User WHERE username = '$user' && password = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) 
        {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) 
        {
            $arr[] = array (
                "user" => $row['username'],
                "pass" => $row['password']
            );
        }

        $json = json_encode(array($arr));

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        else
        {
            $_SESSION['login'] = FALSE;
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
            
        }
    }
}
?>