<?php   include_once('../functions/functions.php'); 
        session_start();
        // logout();
?>
<!DOCTYPE html>
<html>

    <head> 
        <title> COMP6002-9999413-Assessment1 </title>
        
        <link rel="stylesheet" href="css/main.css" type="text/css" >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>

    <body>
                <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?> 

        <img  class="Logo" src="images/link.png" alt="logo">
        <h1 class="header1">it worked</h1>
        <img class="header" src="images/deco.png" alt="decoration">

        <div class="col-lg-12">      
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="collapse-navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="links.html">Quick Links</a></li>
                            <li><a href="info.html">Information</a></li>
                            <li><a href="pathways.html">Pathways</a></li>
                            <li><a href="admin.php">Login</a></li>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
        </div>



<!--NavTable-->
        <div class="col-sm-12">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a href="index.html" >Home</a></li>
                <li role="presentation"><a href="links.html">Quick Links</a></li>
                <li role="presentation"><a href="info.html">Information</a></li>
                <li role="presentation"><a href="pathways.html">Pathways</a></li>
                <li role="presentation"><a href="admin.php">Login</a></li>
            </ul>
        </div>



        <div class="row">
            <div class="col-md-4">
                    <h1 class="welcometitle"> Welcome to bcs.net.nz </h1>
            </div>
        </div>

        <br><br>

        <div class="row">
            <div class="col-md-5">
                <p class="homeP"> Toi-Ohomai Institute of Technology & University of Waikato<br>
                Bachelor of Computing and Mathematical Science </p>
                <br>
                <p class="homeP">This Intranet is for students who are completing the Bachelor of Computer Science in Applied Science.
                <p class="homeP">  Successfully complete the Diploma in Applied Computing (Level 5) and (Level 6) and gain full credit for the first and second year of this three year University of Waikato degree </P>
            </div>  

            <div class="col-lg-5">
                <div class="slides-frame">
                    <div class="slides">
                        <div class="slide-1 slide">BongardCenter</div>
                        <div class="slide-2 slide">BongardCenter</div>
                        <div class="slide-3 slide">BongardCenter</div>
                        <div class="slide-4 slide">BongardCenter</div>
                    </div>
                </div>
            </div>
        </div>
        <?php  
        }
        else
        {
        ?>
                <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../login.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>


        <a href="https://www.boppoly.ac.nz/">  <img class="toi-logo" src="images/link.png" alt="toi-logo-link"> </a>
        <a href="http://www.waikato.ac.nz/"> <img class="waikato" src="images/waikato.png" alt="waikato-logo"> </a>


        <script src="js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGAiret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
       
    </body>

</html>